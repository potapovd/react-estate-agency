# React Estate Agency  #


DEMO
=========
[React Estate Agency](https://react-estate-agency.firebaseapp.com/)

----------

CHANGELOG
=========

### **0.1.0 - 05.02.2018** 
 - initial release



----------


INSTALLATION
=========

### install dependencies
### `npm install` 

### serve with hot reload at localhost:8080
### `npm run dev`

### build for production with minification
### `npm run prod`
