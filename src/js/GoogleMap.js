import React from 'react'
import PropTypes from 'prop-types'

class GooleMap extends React.Component{
	constructor(props){
		super(props);
		this.state={
			markers:[]
		}
	}

	componentWillReceiveProps(nextProps){
		const {activePoperty, filteredProperties, isFiltering} = nextProps;
		const {index} = activePoperty;
		if( isFiltering && filteredProperties.length===0  ){
			this.hideAllIW();
		}else{
			this.hideAllIW();
			this.showIW(index);
		}

	}

	componentDidUpdate(){
		const {filteredProperties,isFiltering} = this.props;
		const {markers} = this.state;

		markers.forEach(marker=>{
			const {property} = marker;
			if(isFiltering){

				if(filteredProperties.includes(property)){
					markers[property.index].setVisible(true)
				}else{
					markers[property.index].setVisible(false)
				}

			}else{
				markers[property.index].setVisible(true)
			}
		})
	}

	showIW(index){
		const {markers} = this.state;
		markers[index] && markers[index].iw.open(this.map,markers[index]);

	}
	hideAllIW(){
		const {markers} = this.state;
		markers.forEach(marker => {
			marker.iw.close();
		});
	}

	componentDidMount(){
		const {properties, activePoperty} = this.props;
		//const {latitude,longitude} = activePoperty;
		this.map = new google.maps.Map(this.refs.map, {
			center: {lat: 47.385454, lng:8.524523},
			//center: {lat:latitude, lng:longitude},
			mapTypeControl:false,
			zoom: 13
		});

		this.createMarkers(properties)

	}

	createMarkers(properties){

		const {setActiveProperty, activePoperty} = this.props;
		const activePopertyIndex = activePoperty.index;
		const {markers} = this.state;


		properties.map(property =>{
			const {latitude, longitude, index, address} = property;

			if(activePopertyIndex===index){
				this.marker = new google.maps.Marker({
					position: {lat:latitude, lng:longitude},
					map: this.map,
					animation: google.maps.Animation.DROP,
					label:{
						color:'#ffffff',
						text:`${index+1}`
					},
					property
				});
			}else{
				this.marker = new google.maps.Marker({
					position: {lat:latitude, lng:longitude},
					map: this.map,
					label:{
						color:'#ffffff',
						text:`${index+1}`
					},
					property
				});
			}



			// create info popup
			const iw = new google.maps.InfoWindow({
				content:`<h1>${address}</h1>`
			});

			this.marker.iw = iw;


			this.marker.addListener('click',function () {

				//close other popos
				this.hideAllIW();

				setActiveProperty(property,true);
			}.bind(this));

			markers.push(this.marker);

			//active popup
			this.showIW(activePopertyIndex)

		})

	}

	render(){
		return(
			<div className="mapContainer">
				<div id="map" ref="map"></div>
			</div>
		)
	}


}

GooleMap.propTypes = {
	properties: PropTypes.array.isRequired,
	setActiveProperty:PropTypes.func.isRequired,
	activePoperty:PropTypes.object.isRequired,
	filteredProperties: PropTypes.array,
	isFiltering: PropTypes.bool.isRequired
}

export default GooleMap;