import React from 'react'
import PropTypes from 'prop-types'
import {priceFormat} from './utils/Formatters'


class Filter extends React.Component{

	render(){
		const {toggleFilter, handleFilterChange, clearFilter} = this.props;
		return(
			<form ref={input => this.form = input} className="filter">
				<div className="row">
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox">
							<label htmlFor="filterBedrooms">
								<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Bedrooms
							</label>
							<select className="form-control" id="filterBedrooms" name="filterBedrooms" onChange={(e)=>handleFilterChange(e)}>
								<option value="any">Any</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
						</div>
					</div>
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox">
							<label htmlFor="filterBathrooms">Bathrooms</label>
							<select className="form-control" id="filterBathrooms" name="filterBathrooms" onChange={(e)=>handleFilterChange(e)}>
								<option value="any">Any</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
						</div>
					</div>
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox">
							<label htmlFor="filterCars">Car Spaces</label>
							<select className="form-control" id="filterCars" name="filterCars" onChange={(e)=>handleFilterChange(e)}>
								<option value="any">Any</option>
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
						</div>
					</div>
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox filterFrom">
							<label htmlFor="priceFrom">Min Price</label>
							<select className="form-control" id="priceFrom" name="priceFrom" onChange={(e)=>handleFilterChange(e)}>
								<option value="0">Any</option>
								<option value="500000">{priceFormat(500000)}</option>
								<option value="600000">{priceFormat(600000)}</option>
								<option value="700000">{priceFormat(700000)}</option>
								<option value="800000">{priceFormat(800000)}</option>
								<option value="900000">{priceFormat(900000)}</option>
							</select>
						</div>
					</div>
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox">
							<label htmlFor="priceTo">Max Price</label>
							<select className="form-control" id="priceTo" name="priceTo" onChange={(e)=>handleFilterChange(e)}>
								<option value="1000001">Any</option>
								<option value="600000">{priceFormat(600000)}</option>
								<option value="700000">{priceFormat(700000)}</option>
								<option value="800000">{priceFormat(800000)}</option>
								<option value="900000">{priceFormat(900000)}</option>
								<option value="1000000">{priceFormat(1000000)}</option>
							</select>
						</div>
					</div>
					<div className="col-xs-12 col-sm-4">
						<div className="form-group filterBox">
							<label htmlFor="filterSort">Order by</label>
							<select className="form-control" id="filterSort" name="filterSort" onChange={(e)=>handleFilterChange(e)}>
								<option value="any">Default</option>
								<option value="0">Price: - Low to High</option>
								<option value="1">Price: - High to Low</option>
							</select>
						</div>
					</div>
					<div className="form-group filterBox">
						<label>&nbsp;</label>
						<button className=" btn btn-primary btn-clear" onClick={(e)=>clearFilter(e, this.form)}>
							<span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Clear
						</button>
					</div>
				</div>


				<button className="btn-filter" onClick={(e)=>toggleFilter(e)}>

					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
			</form>
		)
	}
}

Filter.propTypes = {
	toggleFilter: PropTypes.func.isRequired,
	clearFilter: PropTypes.func.isRequired,
	handleFilterChange: PropTypes.func.isRequired
}

export default Filter;
