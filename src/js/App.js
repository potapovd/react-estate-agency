import React from 'react';

import Card from './Card'
import GoogleMap from './GoogleMap'
import data from './data/Data'
import jump from 'jump.js'
import {easeInOutCubic} from "./utils/Easing";
import Header from './Header'
import noItemsImg from '../images/location-map.svg'

class App extends React.Component {

  constructor(props){
    super(props);

    this.state={
	    properties: data.properties,
      activePoperty:data.properties[0],
	    filterIsVisibile:false,
	    filterBedrooms:'any',
	    filterBathrooms:'any',
	    filterCars:'any',
	    priceFrom:500000,
	    priceTo:1000000,
	    filterSort:'any',
	    filteredProperties:[],
	    isFiltering: false
    };
    this.setActiveProperty = this.setActiveProperty.bind(this);
    this.toggleFilter = this.toggleFilter.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.filterProperties = this.filterProperties.bind(this)
    this.clearFilter = this.clearFilter.bind(this)
  }



  handleFilterChange(e){
  	const target = e.target;
  	const {value,name} = target;
	  this.setState({
		  [name]:value
	  }, function () {
		  this.filterProperties();
	  })
  }

	filterProperties(){
		const {properties, filterBedrooms, filterBathrooms, filterCars, filterSort, priceFrom, priceTo } = this.state;
		const isFiltering =
				filterBedrooms !== 'any' ||
				filterBathrooms !== 'any' ||
				filterCars !== 'any' ||
				filterSort !== 'any' ||
				priceFrom !== '0' ||
				priceTo !== '1000001'
		;


		const getFilteredProperties = (properties) =>{
			const filteredProperies = [];
			properties.map(property => {
				const {bedrooms,bathrooms,carSpaces,price} = property;
				const match =
					(bedrooms === parseInt(filterBedrooms) || filterBedrooms === 'any') &&
					(bathrooms === parseInt(filterBathrooms) || filterBathrooms === 'any') &&
					(carSpaces === parseInt(filterCars) || filterCars === 'any') &&
					(price >= priceFrom && price <= priceTo);

				match && filteredProperies.push(property)
			});

			//sort
			switch(filterSort){
				case '0':
					filteredProperies.sort((a,b) => a.price - b.price);
					break;
				case '1':
					filteredProperies.sort((a,b) => b.price - a.price);
					break;
			}

			filteredProperies[0] && this.setState({activePoperty:filteredProperies[0]});

			  return filteredProperies
		}

		this.setState({
			filteredProperties: getFilteredProperties(properties),
			activeProperties:  getFilteredProperties(properties)[0] || properties[0],
			isFiltering
		})
	}

  toggleFilter(e){
		e.preventDefault();
		this.setState({
			filterIsVisibile: !this.state.filterIsVisibile
		})
  }

	clearFilter(e,form){
		e.preventDefault();
		this.setState({
			pProperties: this.state.properties.sort((a,b)=>a.index - b.index),
			filterBedrooms:'any',
			filterBathrooms:'any',
			filterCars:'any',
			filterSort:'any',
			priceFrom:500000,
			priceTo:1000000,
			filteredProperties:[],
			isFiltering: false,
			activePoperty: this.state.properties[0]
		});
		form.reset();
	}

	setActiveProperty(property, scroll){
    const {index} = property;
    this.setState({activePoperty:property});


    if(scroll){
	    //scroll to item
	    const target = `#card-${index}`;
	    jump(target,{
		    duration:1000,
		    easing:easeInOutCubic
	    })
    }


  }

  render(){
    const {properties,activePoperty,filterIsVisibile,filteredProperties,isFiltering,filterSort} = this.state;
    const propertiesList = isFiltering ? filteredProperties : properties;

    return (
      <div>
        {/* listings - Start */}
        <div className="listings">

          {/* Header - Start - add .filter-is-visible to show filter*/}
          <Header
	          filterIsVisibile = {filterIsVisibile}
	          toggleFilter={this.toggleFilter}
	          clearFilter={this.clearFilter}
	          handleFilterChange = {this.handleFilterChange}
          />
          {/* Header - End */}

          <div className="cards container">
            <div className={`cards-list row ${propertiesList.length===0 ? ' is-empty':''}`}>
              
              {
	              propertiesList.map(property=>{
		              return <Card
                    key={property._id}
                    property={property}
			              activePoperty = {activePoperty}
			              setActiveProperty = {this.setActiveProperty}
                  />
                })
              }
	            {
		            (isFiltering && propertiesList.length === 0) && <p className="warning"><img src={noItemsImg} alt=""/><br/>No Items were found</p>
	            }


            </div>
          </div>
        </div>
        {/* listings - End */}

        {/* mapContainer - Start */}
        <GoogleMap
          properties={properties}
          activePoperty={activePoperty}
          setActiveProperty={this.setActiveProperty}
	        filteredProperties = {filteredProperties}
	        isFiltering = {isFiltering}
        />

        {/* mapContainer - End */}
      </div>
    )
  }
}

export default App;