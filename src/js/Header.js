import React from 'react'
import image from '../images/house-location-pin.svg'
import PropTypes from 'prop-types'
import Filter from './FilterHeader'

const Header = ({filterIsVisibile,toggleFilter,handleFilterChange,clearFilter})=> {
	return(
		<header className={`${filterIsVisibile ? 'filter-is-visible':''}`}>

			{/* Filter - Start */}
			<Filter
				toggleFilter={toggleFilter}
				handleFilterChange={handleFilterChange}
				clearFilter={clearFilter}
			/>
			{/* Filter - End */}

			<img src={image} />
			<h1>Estate Agency</h1>
			<button className="btn-filter" onClick={(e)=>toggleFilter(e)}>
				<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Filter
			</button>
		</header>
	)
};

Header.propTypes = {
	filterIsVisibile: PropTypes.bool.isRequired,
	toggleFilter: PropTypes.func.isRequired,
	clearFilter: PropTypes.func.isRequired,
	handleFilterChange: PropTypes.func.isRequired
};

export default Header;
